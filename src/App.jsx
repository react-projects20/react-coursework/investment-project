import Header from "./compents/Header"
import Results from "./compents/Results";
import UserInput from "./compents/UserInput"
import { useState } from "react";

function App() {
  
  //This ls lifting the state up we removed from UserInput.jsx and we will
  //pass it down to the Results.jsx both the useState & handleChange function 
  const [userInput, setUserInput] = useState({
    initialInvestment: 10000,
    annualInvestment: 1200,
    expectedReturn: 6,
    duration: 10,
});

const inputIsValid = userInput.duration >= 1;


function handleChange(inputIdentifier, newValue) {
  setUserInput((prevUserInput) => {
      return { ...prevUserInput, 
        [inputIdentifier]: + newValue };
  });
}

  return (
    <div>
      <Header />
      <UserInput userInput={userInput} onChange={handleChange}/>
      {!inputIsValid && <p className="center">Please enter valid  inputs</p>}
      {inputIsValid && <Results input={userInput} />}
    </div>

  )
}

export default App
